<?php
/**
 * Template English lexicon topic
 *
 * @language en
 * @package modx
 * @subpackage lexicon
 */

$_lang['access'] = 'Access';
$_lang['filter_by_category'] = 'Filter by Category...';
$_lang['rank'] = 'Rank';
$_lang['template'] = 'Template';
$_lang['template_assignedtv_tab'] = 'Assigned Template Variables';
$_lang['template_code'] = 'Template code (HTML)';
$_lang['template_desc'] = 'Description';
$_lang['template_desc_category'] = 'The Category this Template belongs in.';
$_lang['template_desc_description'] = 'A short description of this Template.';
$_lang['template_desc_name'] = 'The name of this Template.';
$_lang['template_delete_confirm'] = 'Are you sure you want to delete this template?';
$_lang['template_duplicate_confirm'] = 'Are you sure you want to duplicate this template?';
$_lang['template_edit_tab'] = 'Edit Template';
$_lang['template_empty'] = '(empty)';
$_lang['template_err_default_template'] = 'This template is set as the default template. Please choose a different default template in the MODX configuration before deleting this template.<br />';
$_lang['template_err_delete'] = 'An error occurred while deleting the template.';
$_lang['template_err_duplicate'] = 'An error occured while duplicating the template.';
$_lang['template_err_exists_name'] = 'A template already exists with the name "[[+name]]".';
$_lang['template_err_in_use'] = 'This template is in use. Please set the documents using the template to another template. Documents using this template:<br />';
$_lang['template_err_locked'] = 'Template is locked from editing.';
$_lang['template_err_nf'] = 'Template not found!';
$_lang['template_err_ns'] = 'Template not specified.';
$_lang['template_err_ns_name'] = 'Please specify a name for the template.';
$_lang['template_err_remove'] = 'An error occurred while removing the template.';
$_lang['template_err_save'] = 'An error occurred while saving the template.';
$_lang['template_icon'] = 'Icon';
$_lang['template_icon_description'] = 'Optional. A custom icon class for all resources with this template.';
$_lang['template_lock'] = 'Lock template for editing';
$_lang['template_lock_msg'] = 'Users must have the edit_locked attribute to edit this template.';
$_lang['template_locked_message'] = 'This template is locked.';
$_lang['template_management_msg'] = 'Here you can choose which template you wish to edit.';
$_lang['template_msg'] = 'Create and edit templates. Changed or new templates won\'t be visible in your site\'s cached pages until the cache is emptied; however, you can use the preview function on a page to see the template in action.';
$_lang['template_name'] = 'Template name';
$_lang['template_new'] = 'New Template';
$_lang['template_no_tv'] = 'No template variables have been assigned to this template yet.';
$_lang['template_properties'] = 'Default Properties';
$_lang['template_reset_all'] = 'Reset all pages to use Default template';
$_lang['template_reset_specific'] = 'Reset only \'%s\' pages';
$_lang['template_title'] = 'Create/edit template';
$_lang['template_tv_edit'] = 'Edit the sort order of the template variables';
$_lang['template_tv_msg'] = 'The template variables assigned to this template are listed below.';
$_lang['template_untitled'] = 'Untitled Template';
$_lang['templates'] = 'Templates';
$_lang['tvt_err_nf'] = 'Template Variable does not have access to the specified Template.';
$_lang['tvt_err_remove'] = 'An error occurred while trying to remove the template variable from the template.';


// Custom
$_lang['site_description'] = 'Alexey Sherbachev’s studio of modern design';
$_lang['interior_workshop'] = 'INTERIOR WORKSHOP';
$_lang['who_we_are'] = 'Who are we';
$_lang['who_we_are_text'] = 'We are a collective of creative people, architects and designers. our team make, designs and it can be said that "produces" non-trivial objects for extraordinary clients';
$_lang['private_interiors'] = 'Private interiors';
$_lang['public_interiors'] = 'Public interiors';
$_lang['exteriors'] = 'Exteriors';
$_lang['decor'] = 'Decor';
$_lang['portfolio'] = 'Portfolio';
$_lang['all'] = 'All';
$_lang['see_all_projects'] = 'See all projects';
$_lang['process'] = 'Process';
$_lang['project_from_idea'] = 'Project from idea to realization';
$_lang['go_to_project'] = 'Go to the project';
$_lang['what_we_do'] = 'Our workshop is engaged in designing the objects’ interior in Ukraine and abroad';
$_lang['contact_to_email'] = 'If you are interested to contact me - please send an email and I will answer you soon';
$_lang['publications'] = 'Publications';
$_lang['articles_about_us'] = 'Articles about us in the press';
$_lang['ideology'] = 'Ideology';
$_lang['about_me'] = 'About me';
$_lang['alexey'] = 'Alexey Sherbachev';
$_lang['main_position'] = 'chief of the studio';
$_lang['articles'] = 'Articles';
$_lang['latest_updates_blog'] = 'Latest updates on the blog';
$_lang['contacts'] = 'Contacts';
$_lang['write_to_us'] = 'Write or simply come to us';
$_lang['copy'] = 'Copyright © InCube 2015';
$_lang['address'] = 'Kiev, Velyka Vasylkivska St. 13/1';
$_lang['your_name'] = 'Enter your name';
$_lang['your_email'] = 'Your email';
$_lang['text_of_message'] = 'Text of message';