// MAIN.JS
//--------------------------------------------------------------------------------------------------------------------------------
//This is main JS file that contains custom JS scipts and initialization used in this template*/
// -------------------------------------------------------------------------------------------------------------------------------
// Template Name: Luther.
// Author: Unbranded.
// Version 1.0 - Initial Release
// Website: http://www.unbranded.co 
// Copyright: (C) 2015 
// -------------------------------------------------------------------------------------------------------------------------------

/*global $:false */
/*global window: false */

(function(){
  "use strict";

    //Vieport height calculation
    $('.full-height').height($(window).height());
    $('.full-width').width($(window).width());

    var headerH = $('.header-content').outerHeight();
        $('.home-full-height').height($(window).height()-headerH);

        $('.home-margin-top').css('margin-top', headerH);
        $('.home-margin-bottom').css('margin-bottom', headerH);



    $(window).load(function(){


            //Directs inner-page menu links to correspond main page section
            var locationUrl = $(location).attr('href');
            var targetLocation = locationUrl.split('#');
            var targetId = '#'+targetLocation[1];
                $('.main-header li > a[href='+targetId+']').trigger('click');


            // To verticaly center toggle-menu
            //Applies "logo" height to "menu-wrap"    
            var menu_H = $(".logo").outerHeight();
                $(".menu-wrap").css('height', menu_H);   

            //Applies "toggle-menu" height to "menu-wrap-vertical-align"
            var t_menu_H =$('#toggle-menu').height();
                $(".menu-wrap .vertical-align").css('height', t_menu_H);



            //Applies "services-left-panel" height to "services-right-panel"
            var ser_H = $(".services-left-panel").outerHeight();
                  $(".services-right-panel").css('height', ser_H);


            //Applies "clients-wrap" height to "clients-text-content-wrap"
            var client_H = $(".clients-wrap").outerHeight();
                  $(".clients-text-content-wrap").css('height', client_H);


            //Applies "clients-wrap" height to "clients-text-content-wrap"
            var sec_Head_H = $(".section-header-name").height();
                  $(".section-header-text").css('height', sec_Head_H);




        setTimeout(function(){


            //Applies "portfolio-item" height to "portfolio-item-mask"
            var port_H = $(".portfolio-item img").height();
                  $(".portfolio-item-mask").css('height', port_H);   

            var port_mask_H = $(".portfolio-mask-content").height();
                  $(".portfolio-item-mask .vertical-align").css('height', port_mask_H);
                  $(".portfolio-mask-content").css('bottom', - port_mask_H);
       
            //Applies "bx-pager-button" height and width "pager-item-overlay"
             var bx_H = $(".bx-pager-item").height();
             var bx_W = $(".bx-pager-item").width();
                  $(".pager-item-overlay").css('height', bx_H);
                  $(".pager-item-overlay").css('width', bx_W);


        },1000);      



            if( !device.tablet() && !device.mobile() ) {

                  //Applies "container" width to "sub-menu"
                  $(".sub-menu").css('width',$('.container').width());  

                  $('.main-menu-item a').on('click', function(){
                      $('#toggle-menu').trigger('click');
   
                  });          
            } 


            else 

            {     
                  var winH = $(window).height();
                  var winW = $(window).width();
                  

                  if(winH < winW)
                    $('.to-down-wrap').css('display', 'none');

                  //Mobile-Nav, Mobile-Nav Submenu Action
                  $('.sub-menu').hide();

                  $('.has-sub-menu a').on('click', function(){

                        var this_menu_item = $(this);

                        if(!this_menu_item.data('sub-menu-open'))

                            {
                                $('.standard-nav li.has-sub-menu a').data('sub-menu-open', false);
                                $('.standard-nav li.has-sub-menu a').removeClass('sub-menu-open'); 
                                this_menu_item.closest('li').children('.sub-menu').first().stop().slideDown(500); 
                                this_menu_item.addClass('sub-menu-open');
                                this_menu_item.data('sub-menu-open', true);
                        
                                return false;
                            }
                        else
                            {
                              this_menu_item.closest('li').children('.sub-menu').first().stop().slideUp(500);
                              this_menu_item.removeClass('sub-menu-open');
                              this_menu_item.data('sub-menu-open', false);
                              return true;
                            }

                  });

                  $('.sub-menu-open a').on('click',function(){
                    return true;

                  });

                          
                  $('.main-menu li:not(.has-sub-menu) a').on('click',function(){
                      $('#toggle-menu').trigger('click');
                  });
          
            }


           
  
         // Highlight Menu item on scroll
         var page_stack = $.makeArray();
            var stack_top = 0;

            $('.main-menu.main-home-menu li:first-child > a').addClass('active-nav');      

            $('.nav-highlight').waypoint(function (direction) {
      
                if (direction === 'down') 
                    {  
                       $('.main-menu-item > a').removeClass('active-nav');
                       $('.main-menu-item a[href=#'+ $(this).attr('id') +']').addClass('active-nav');
                       stack_top = stack_top+1; 
                       page_stack[stack_top] = $(this).attr('id');           
                    } 
                else 
                    {
                      stack_top = stack_top-1;
                      $('.main-menu-item > a').removeClass('active-nav');
                      $('.main-menu-item a[href=#'+page_stack[stack_top]+']').addClass('active-nav');   
                    }

            },{ offset: 300 });



          // Highlight Slim menu item on scroll
          var mobile_page_stack = $.makeArray();
              var mobile_stack_top = 0;

              $('.slimmenu ul li:first-child > a').addClass('active-nav');      

              $('.nav-highlight').waypoint(function (direction) {
          
                  if (direction === 'down') 
                      {
                         $('.slimmenu li > a').removeClass('active-nav');
                         $('.slimmenu li a[href=#'+ $(this).attr('id') +']').addClass('active-nav');
                         mobile_stack_top = mobile_stack_top+1; 
                         mobile_page_stack[mobile_stack_top] = $(this).attr('id');           
                      } 
                  else 
                      {
                        mobile_stack_top = mobile_stack_top-1;
                        $('.slimmenu li > a').removeClass('active-nav');
                        $('.slimmenu li a[href=#'+mobile_page_stack[mobile_stack_top]+']').addClass('active-nav');   
                      }

              },{ offset: 300 });




                 $(".idea-slider").owlCarousel({
                    slideBy: 2,
                    loop:true,
                    margin:10,
                    dots: false,
                    nav:true,
                    autoHeight: true,
                    scrollPerPage: true,
                    navText : [
                            "<i class='icon ion-android-arrow-back'></i>",
                            "<i class='icon ion-android-arrow-forward'></i>"
                          ],
                          
                    responsive:{
                        0:{
                            items:1
                        },
                        415:{
                            items:1
                        },
                        480:{
                            items:2
                        },
                        739:{
                            items:2
                        },
                        1000:{
                            items:2
                        }
                    }
                
                });

                 $(".public-slider").owlCarousel({
                    loop:true,
                    margin:10,
                    dots: false,
                    nav:true,
                    autoHeight: true,
                    navText : [
                            "<i class='icon ion-android-arrow-back'></i>",
                            "<i class='icon ion-android-arrow-forward'></i>"
                          ],
                          
                    responsive:{
                        0:{
                            items:1
                        },
                        415:{
                            items:1
                        },
                        480:{
                            items:3
                        },
                        739:{
                            items:3
                        },
                        1000:{
                            items:3
                        }
                    }
                
                });
   

    });



	  $(document).ready(function() { 
 

          // CountTo Initialization on click
            $('.bx-pager-button').on('click',function(){

                $('.timer').countTo({
                    speed: 2000
                });

            });


            // CountTo Initialization on scroll
            var count = false;
            
            $('.count').waypoint(function (direction){
      
                if (direction === 'down') {

                    if(count === false)
                    {
                   
                      $('.timer').countTo({
                        speed: 2000
                        });
                    count = true;
                    }                      
                }

            }, { offset: 800 });




            //Main header position adjustment on scroll
              $('#about').waypoint(function (direction) {
            
                  if (direction === 'down') 
                  {
                      $('.main-header.bottom').addClass('header-fix'); 
                      $('.main-header.bottom').removeClass('header-bottom');            
                  } 
                  else 
                  {  
                      $('.main-header.bottom').removeClass('header-fix');
                      $('.main-header.bottom').addClass('header-bottom');   
                  }
              },{ offset:70 });


            // To-top position adjustment on scroll
              $('#master-wrap').waypoint(function (direction) {

                    
                if (direction === 'down') 
                    
                    $(".to-top, .babel-links").animate({bottom:'40px', opacity: '1'});

                else 
                      
                    $(".to-top, .babel-links").animate({bottom:'-40px', opacity: '0'});
                  
                 
            },{ offset: -300 }); 
      

          //Open new window
            $('post-share .social-icons a').click(function(){
                var link_url = $(this).attr('href');
                window.open(link_url, 'newwindow', 'width=650, height=500, top=200, left=150'); 
                return false;
            });



          // Lightbox Initialization

            $('.video').featherlight({iframeMaxWidth: '100%', iframeWidth: 500,iframeHeight: 300});

            $('.gallery').featherlightGallery({
                previousIcon: "<i class='icon ion-arrow-left-c'></i>",     /* Code that is used as previous icon */
                nextIcon: "<i class='icon ion-arrow-right-c'></i>",        /* Code that is used as next icon */
                closeIcon: "<i class='icon ion-android-close'></i>",        /* Code that is used as close icon */ 
                openSpeed:    300,
                closeSpeed:   300
            });



          //Bxslider Intialization
             var bx_slider_H = $(".bxslider").outerHeight();
                  $(".bx-wrapper").css('height', bx_slider_H);  

             
             $('.about-slider').bxSlider({
                pagerCustom: '#bx-pager',
                pagerLocation: 'top',
                adaptiveHeight: 'ture',
                autoStart: true,
                controls: false 
              });



            //Services Panel  
              $('.services-right-panel .services-item').hide().eq(0).show();
              $('.services-triggers li').eq(0).addClass('active');
             
              $('.services-triggers li').click(function () {
               
                  $('.services-right-panel .services-item').hide();

                  var num = $('.services-triggers li').index(this);

                  $('.services-triggers li').removeClass('active');
                  $('.services-triggers li').eq(num).addClass('active');
                  
                  $('.services-right-panel .services-item').hide().eq(num).fadeIn(2000);
                  return false;
              });
     
            
            // Remove place holder on focus
                $('input,textarea').focus(function(){
                    $(this).data('placeholder',$(this).attr('placeholder'));
                    $(this).attr('placeholder','');
                });

            // Add place holder on blur
                $('input,textarea').blur(function(){
                    $(this).attr('placeholder',$(this).data('placeholder'));
               });


            // Contact Form Ajax Section

                /*$('#contactform').submit(function(){
                      $('.md-content').hide();
                      $('.launch_modal').trigger("click");
                      //alert(1);
                      $.ajax({
                        type: $("#contactform").attr('method'),
                        url: $("#contactform").attr('action'),
                        data: $("#contactform").serialize(),
                        success: function(data) {
                          if(data == 'success')
                          {
                              
                              $('#contactform').each(function(){ 
                                this.reset();
                              });

                              $('#contactform input#name').attr('placeholder',$('#contactform input#name').data('placeholder'));
                              $('#contactform input#name').removeClass('error-msg');

                               $('#contactform input#email').attr('placeholder',$('#contactform input#email').data('placeholder'));
                              $('#contactform input#email').removeClass('error-msg');

                               $('#contactform textarea#message').attr('placeholder',$('#contactform textarea#message').data('placeholder'));
                              $('#contactform textarea#message').removeClass('error-msg');
                              
                              $('.md-content').show();
                          }
                          else
                          {
                            $('.md-content').show();
                            $('.md-content h3').html('Something went wrong!');
                            $('.md-content p').html('Please try again.');
                          }
                        }
                      });
                      return false;
                  });*/




              // Owl Carousel section    


                 $(".social-slider").owlCarousel({
                    loop:true,
                    margin:10,
                    items:4,
                    dots: false,
                    nav:true,
                    autoHeight: true,
                    navText : [
                            "<i class='icon ion-ios-arrow-thin-left'></i>",
                            "<i class='icon ion-ios-arrow-thin-right'></i>"
                          ],

                    responsive:{
                        0:{
                            items:1
                        },
                        480:{
                            items:2
                        },
                        600:{
                            items:3
                        },
                        751:{
                            items:3
                        },
                        1000:{
                            items:4
                        }
                    }
                });


                 $(".news-slider").owlCarousel({
                    loop:true,
                    dots: false,
                    nav:true,
                    navText : [
                            "<i class='icon ion-ios-arrow-thin-left'></i>",
                            "<i class='icon ion-ios-arrow-thin-right'></i>"
                          ],

                    responsive:{
                        0:{
                            items:1
                        },
                        600:{
                            items:1
                        },
                        1000:{
                            items:2
                        }
                    }
                });

        });


})();

$(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();
});



/*--------------------------------------------------
  GOOGLE ANALITYCS
--------------------------------------------------*/
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

ga('create', 'UA-63509700-4', 'auto');
ga('send', 'pageview');

/*--------------------------------------------------
  Yandex Metrika
--------------------------------------------------*/
(function (d, w, c) {
    (w[c] = w[c] || []).push(function() {
        try {
            w.yaCounter34405395 = new Ya.Metrika({
                id:34405395,
                clickmap:true,
                trackLinks:true,
                accurateTrackBounce:true,
                webvisor:true
            });
        } catch(e) { }
    });

    var n = d.getElementsByTagName("script")[0],
        s = d.createElement("script"),
        f = function () { n.parentNode.insertBefore(s, n); };
    s.type = "text/javascript";
    s.async = true;
    s.src = "https://mc.yandex.ru/metrika/watch.js";

    if (w.opera == "[object Opera]") {
        d.addEventListener("DOMContentLoaded", f, false);
    } else { f(); }
})(document, window, "yandex_metrika_callbacks");
