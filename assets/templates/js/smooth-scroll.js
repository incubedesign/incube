
/*global $:false */
/*global window: false */

(function(){
  "use strict";


    $(document).ready(function() { 

        //SMOOTH TOP DOWN SCROLLING

        if( !device.tablet() && !device.mobile() ) {

            //Desktop Navigation Scroll
            $(document).ready(function() {

                $(".scroll").click(function() {
                    var ScrollOffset = 75;
                    //alert(ScrollOffset);
                    $("html, body").animate({
                        scrollTop: $($(this).attr("href")).offset().top-ScrollOffset + "px"
                    }, {
                        duration: 1500,
                        easing: "linear"
                    });
                    return false;
                });

            });
                        
        } 

        else 

        {
                    
            //Mobile Navigation Scroll
            $(document).ready(function() {

                $(".scroll").click(function() {
                    var ScrollOffset = 48;
                    //alert(ScrollOffset);
                    $("html, body").animate({
                        scrollTop: $($(this).attr("href")).offset().top-ScrollOffset + "px"
                    }, {
                        duration: 1500,
                        easing: "linear"
                    });
                    return false;
                });

            });
                   
        }

    });  

})();
//  JSHint wrapper $(function ($)  : ends    