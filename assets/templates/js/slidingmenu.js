// SLIDINGMENU.JS
//--------------------------------------------------------------------------------------------------------------------------------
//JS for operating the sliding menu*/
// -------------------------------------------------------------------------------------------------------------------------------
// Author: Unbranded.
// Website: http://www.unbranded.co 
// Copyright: (C) 2014 
// -------------------------------------------------------------------------------------------------------------------------------


/*global $:false */
/*global window: false */

(function(){
  "use strict";


  	$(document).ready(function() { 


  		// Menu Action

			$('.section-wrap').on('click', function(){
				var menu_icon = $('#toggle-menu');

	    		if (menu_icon.hasClass('toggle-menu-visible'))
	    			{
		    			$('#toggle-menu').trigger('click');
		    		}
			});
			

			$('.standard-nav').hide();

			$('#toggle-menu').on('click', function(){

				$(this).toggleClass('toggle-menu-visible').toggleClass('toggle-menu-hidden');
				$('.standard-nav').slideToggle(500);
				
			});


	});  

})();
//  JSHint wrapper $(function ($)  : ends

