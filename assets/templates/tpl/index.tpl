[[$head]]

<body>
  <div id="preloader">
    <div id="status"></div>
  </div>
  <section id="master-wrap" class="master-wrap">
    [[!randombg]]
    [[$header]]
    <section class="section-wrap">
      [[$paralax]]
      [[$services]]
      [[$portfolio]]
      [[$proccess]]
      [[$mail]]
      [[$public]]
      [[$social_links]]
      [[$articles]]
      [[$about]]
      [[$contact]]
      [[$footer]]
    </section>
  </section>
  [[$scripts]]
</body>
</html>